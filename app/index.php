<?php
  require_once "../inc/lang.php";
  require_once "../inc/sparks.php";

  $lang = 'en';
  $navigator_lang = getDefaultLanguage();

  if($navigator_lang == 'es' || $navigator_lang == 'es-es') $lang = 'es';


  get_topics_sparklines();
?>

<!doctype html>
<html lang="<?php echo $lang; ?>" ng-app="myApp">
<head>
  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
  <meta name="HandheldFriendly" content="true">
  <meta name="apple-touch-fullscreen" content="YES">
  <link href='http://fonts.googleapis.com/css?family=Inconsolata&subset=latin-ext' rel='stylesheet' type='text/css'>


  <title><?php echo $t[$lang]['title']; ?></title>
  <link rel="stylesheet" href="css/yelp.css"/>
  <!-- <link rel="stylesheet" href="css/spaintrends.css" defer> -->

  <base href="/app/"/>

</head>
<body class="ytype jquery ng-cloak" style="font-family: 'Inconsolata'" ng-cloak>

<div id="super-container" style="background: #fffbe5;">
  <h1 style="display:none" class="ylabel ylabel-large" style="color:purple"><?php echo $t[$lang]['h1']; ?></h1>
  <img src="img/spaintrends960.png" border='0' alt="SpainTrends"/>
  <div class="media-story">
	<br/>
    <h2 style="color:purple"><?php echo $t[$lang]['refrain']; ?></h2>
  </div>


<?php
  foreach ($artopics as $key => $topic) {
?>
	#<?php echo $key; ?> <span class="<?php echo $key; ?>">Loading..</span>
<?php
  }
?>


<div class="container_hashtagsh">
  <span ng-controller="HashTagsCntlr as hashtags" style="float:left; width:33%;">
	  <h2><?php echo $t[$lang]['hash_today']; ?></h2>
	  <ul class="island-light">
		<li ng-repeat="ha in hashtags_result track by $index" ng-animate=" 'animate' ">
			#{{ha}}
		</li>
	  </ul>
  </span>

  <span ng-controller="HashTagsWeekCntlr as hashtags" style="float:left; width:33%;">
	  <h2><?php echo $t[$lang]['hash_week']; ?></h2>
	  <ul class="island-light">
		<li ng-repeat="hb in hashtags_result track by $index" ng-animate=" 'animate' ">
			#{{hb}}
		</li>
	  </ul>
  </span>

  <span ng-controller="HashTagsMonthCntlr as hashtags" style="float:left; width:33%;">
	  <h2><?php echo $t[$lang]['hash_month']; ?></h2>
	  <ul class="island-light">
		<li ng-repeat="hc in hashtags_result track by $index" ng-animate=" 'animate' ">
			#{{hc}}
		</li>
	  </ul>
  </span>
</div>

  <br/>
  <div align="right" style="">
	  <a style="color:purple;cursor:pointer" ng-click="show='esp'">Español</a>
	  <a style="color:purple;cursor:pointer" ng-click="show='eng'">English</a>
	  <a style="color:purple;cursor:pointer" ng-click="show='jap'">日本の</a>
	  <a style="color:purple;cursor:pointer" ng-click="show='ara'">العربية</a>
  </div>
  <br/>
  <span ng-controller="ListEsCntlr as list" ng-show="show=='esp' || !show">
	  <h2><?php echo $t[$lang]['latest_tuits']; ?></h2>
	  <table class="table table-stripped">

		<tr ng-repeat="l in list track by $index" ng-animate=" 'animate' "><td ng-hide="!l.text.length">
			<a href="{{l.long_url}}" target="_blank" alt="{{l.long_url}}">{{l.text}}</a>
		</td></tr>
	  </table>

  </span>

  <span ng-controller="ListEnCntlr as list" ng-show="show=='eng'">
	  <h2><?php echo $t[$lang]['latest_tuits']; ?></h2>
	  <table class="table table-stripped">

		<tr ng-repeat="l in list track by $index" ng-animate=" 'animate' "><td ng-hide="!l.text.length">
			<a href="{{l.long_url}}" target="_blank" alt="{{l.long_url}}">{{l.text}}</a>
		</td></tr>
	  </table>

  </span>

  <span ng-controller="ListArCntlr as list" ng-show="show=='ara'">
	  <h2><?php echo $t[$lang]['latest_tuits']; ?></h2>
	  <table class="table table-stripped">

		<tr ng-repeat="l in list track by $index" ng-animate=" 'animate' ">
			<td ng-hide="!l.text.length">
				<a href="{{l.long_url}}" target="_blank" alt="{{l.long_url}}">{{l.text}}</a>
			</td>
		</tr>
	  </table>

  </span>

  <span ng-controller="ListJaCntlr as list" ng-show="show=='jap'">
	  <h2><?php echo $t[$lang]['latest_tuits']; ?></h2>
	  <table class="table table-stripped">

		<tr ng-repeat="l in list track by $index" ng-animate=" 'animate' ">
			<td ng-hide="!l.text.length">
				<a href="{{l.long_url}}" target="_blank" alt="{{l.long_url}}">{{l.text}}</a>
			</td>
		</tr>
	  </table>

  </span>


  <hr>
  <div style="color:purple"><small><?php echo $t[$lang]['amazed'];?></small></div>
  <span style="float:left" ng-controller="UpdatedCntlr"><i>last updated at <time datetime="{{time}}"> {{time}} </time></i></span>
  <span style="float:right" align="right">
	  <a href="https://plus.google.com/u/0/115668598016102687344?rel=author">leandro<script>document.write('@')</script>lean<script>document.write('dro.')</script>org</a>
  </span>

</div>



  <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
  <script src="js/jquery.sparkline.min.js"></script>

  <!-- In production use:
  <script src="//ajax.googleapis.com/ajax/libs/angularjs/x.x.x/angular.min.js"></script>
  -->
  <script src="lib/angular/angular.js"></script>
  <script src="lib/angular/angular-route.js"></script>
  <script src="js/app.js"></script>
  <script src="js/services.js"></script>
  <script src="js/controllers.js"></script>
  <script src="js/filters.js"></script>
  <script src="js/directives.js"></script>





  <script>
	$( document ).ready(function() {
	  <?php $n=0; foreach ($artopics as $key => $topic) { ?>
        var values_<?php echo $n; ?> = [<?php echo $topic;?>];
		$('.<?php echo $key; ?>').sparkline(values_<?php echo $n;  $n++; ?>, { fillColor:'#ECEDED', lineColor: 'purple' , spotColor: '#7997BB', minSpotColor: '', maxSpotColor: '' });

	  <?php } ?>

	});
  </script>








  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1076705-15', 'spaintrends.es');
  ga('send', 'pageview');

</script>
</body>
</html>
