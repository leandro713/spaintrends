<?php

if (!@$link) {
	$link = mysqli_connect("localhost","root","","leandro_alh") or die("Error " . mysqli_error($link));
}


function get_topics_sparklines(){
	global $link;
	global $artopics;

	//topics in CSV
	$topics = @fgetcsv ( @fopen("../inc/topics.csv", "r") );
	//end topics in CSV

	#$topics = array('spain', 'venezuela', 'madrid', 'barcelona', 'crisis');
	if (is_array( $topics) ) {

		foreach ($topics as $topic) {
			$query = "SELECT DATE(FROM_UNIXTIME(time)) AS day,
							COUNT(hashtag) AS hashtag
								 FROM   hashtags_spain
								WHERE hashtag='".$topic."'
								 GROUP BY DATE(FROM_UNIXTIME(time))
								 ORDER BY day
								LIMIT 15";
			$result = $link->query($query);
			while($row = mysqli_fetch_array($result)) {
				$artopics[$topic][] = $row['hashtag'];
			}
		}
		foreach ($artopics as &$topic) {
			$topic = implode(",", $topic);
		}

	}
}
