<?php
header('Content-Type: application/json; charset=UTF-8');
ini_set('date.timezone', 'Europe/Madrid');
date_default_timezone_set("Europe/Madrid");
$artopics = array();

if (!@$link) {
	$link = mysqli_connect("localhost","root","","leandro_alh") or die("Error " . mysqli_error($link));
}

function get_hashtags($days) {
	$hours = ($days * 24);
	$from = time() - ($hours * 60 * 60);
	$query = "SELECT hashtag, count(hashtag) as total FROM hashtags_spain
          WHERE time > '".$from."'
          GROUP BY hashtag
          ORDER BY 2 DESC, 1 LIMIT 10";

    executehashs($query);
}

function get_langs($lang) {
	$query = "SELECT long_url, text from scrapped_spain
				where lang='".$lang."'
				and link IS NOT NULL
				order by created_at DESC
				limit 12";

    executelang($query);
}

function executehashs($query) {
	global $link;
	$result = $link->query($query);
	while($row = mysqli_fetch_array($result)) {
	   $_[] = $row['hashtag'];
	}
	print json_encode( $_);
}

function executelang($query) {
	global $link;
	$result = $link->query($query);
	$i = 0;
	while($row = mysqli_fetch_array($result)) {
	   $_[$i]['text'] = $row['text'];
	   $_[$i]['long_url'] = $row['long_url'];
	   $i++;
	}
	//print_r($_);
	print json_encode( $_ );
}

function last_update(){
  global $link;
  $query = 'SELECT created_at FROM scrapped_spain ORDER BY created_at DESC LIMIT 1';
  $result = $link->query($query);
  while($row = mysqli_fetch_array($result)) {
	print date('Y-m-d\TH:i',$row[0]);
  }
}

/**** TO DO ****/
function is_cached( $name ) {
	$file = "../cache/$name";
	if (file_exists() === FALSE) {
		return FALSE;
	}else{
		$filetime = filectime($file);
		if ( floor((time() - $filetime)/60) <= 10) { // filetime is lower than 10 min
			echo file_get_contents($file);
		}else{                                       // cached file is older than 10 min
			return FALSE;
		}
}

function cache_this ( $name, $val ) {
	$file = "../cache/$name";
	$f = fopen($file, "w");
	fwrite($f, $val);
}
