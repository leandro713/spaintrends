'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', [
  'ngRoute',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers'
]).
config(['$locationProvider', function($location) {
  $location.html5Mode(true); //now there won't be a hashbang within URLs for browers that     support HTML5 history
}]).
config(['$routeProvider', function($routeProvider) {


  //$routeProvider.when('/index', {templateUrl: 'partials/index.html', controller: 'Index'});

  $routeProvider.otherwise({redirectTo: '/index'});

}]);
