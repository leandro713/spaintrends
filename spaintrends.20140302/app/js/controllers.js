'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
  .controller('HashTagsCntlr', ['$scope', '$http', function($scope, $http) {
		$http({
              method: 'GET',
              url: '/api.php?hashtags'
        })
        .success(function (data, status, headers, config) {
			$scope.hashtags_result = data;
			//console.log($scope.hashtags_result);
        });
  }])
  .controller('HashTagsWeekCntlr', ['$scope', '$http', function($scope, $http) {
		$http({
              method: 'GET',
              url: '/api.php?hashtags_week'
        })
        .success(function (data, status, headers, config) {
			$scope.hashtags_result = data;
			//console.log($scope.hashtags_result);
        });
  }])
  .controller('HashTagsMonthCntlr', ['$scope', '$http', function($scope, $http) {
		$http({
              method: 'GET',
              url: '/api.php?hashtags_month'
        })
        .success(function (data, status, headers, config) {
			$scope.hashtags_result = data;
			//console.log($scope.hashtags_result);
        });
  }])
  .controller('ListEsCntlr', ['$scope', '$http', function($scope, $http) {
		$http({
              method: 'GET',
              url: '/api.php?list_es'
        })
        .success(function (data, status, headers, config) {
			$scope.list = data;
        });
  }])
  .controller('ListJaCntlr', ['$scope', '$http', function($scope, $http) {
		$http({
              method: 'GET',
              url: '/api.php?list_ja'
        })
        .success(function (data, status, headers, config) {
			$scope.list = data;
        });
  }])
  .controller('ListArCntlr', ['$scope', '$http', function($scope, $http) {
		$http({
              method: 'GET',
              url: '/api.php?list_ar'
        })
        .success(function (data, status, headers, config) {
			$scope.list = data;
        });
  }])
  .controller('ListEnCntlr', ['$scope', '$http', function($scope, $http) {
		$http({
              method: 'GET',
              url: '/api.php?list_en'
        })
        .success(function (data, status, headers, config) {
			$scope.list = data;
			console.log(data);
        });
  }]);
