<?php
if (!@$link) {
	$link = mysqli_connect("localhost","root","","leandro_alh") or die("Error " . mysqli_error($link));
}

function get_hashtags($days) {
	$hours = ($days * 24);
	$from = time() - ($hours * 60 * 60);
	$query = "SELECT hashtag, count(hashtag) as total FROM hashtags_spain
          WHERE time > '.$from.'
          GROUP BY hashtag
          ORDER BY 2 DESC, 1 LIMIT 10";

    executehashs($query);
}

function get_langs($lang) {
	$query = "SELECT long_url, text from scrapped_spain
				where lang='".$lang."'
				and link IS NOT NULL
				order by created_at DESC
				limit 12";

    executelang($query);
}

function executehashs($query) {
	global $link;
	$result = $link->query($query);
	while($row = mysqli_fetch_array($result)) {
	   $_[] = $row['hashtag'];
	}
	print json_encode( $_);
}

function executelang($query) {
	global $link;
	$result = $link->query($query);
	$i = 0;
	while($row = mysqli_fetch_array($result)) {
	   $_[$i]['text'] = $row['text'];
	   $_[$i]['long_url'] = $row['long_url'];
	   $i++;
	}
	print json_encode( $_ );
}
