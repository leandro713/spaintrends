<?php
require_once "inc/funcs.php";

 switch ($_SERVER['QUERY_STRING']) {
    case 'hashtags':
		get_hashtags(1);
		break;
    case 'hashtags_week':
		get_hashtags(7);
		break;
	case 'hashtags_month':
		get_hashtags(30);
		break;
	case 'list_en':
		get_langs('en');
		break;
	case 'list_es':
		get_langs('es');
		break;
	case 'list_ar':
		get_langs('ar');
		break;
	case 'list_ja':
		get_langs('ja');
		break;
	case 'updated':
		last_update();
		break;

	default:
		break;
 }
